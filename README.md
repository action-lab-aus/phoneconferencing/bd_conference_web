# bd_conference_web

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

###  or for a specific deployment:

```
npm run servecaritas
```

### Compiles and minifies for production
```
npm run build
npm run buildcaritas
```

### Lints and fixes files
```
npm run lint
```

### Check you're on the correct Firebase project
```
firebase projects:list
```

### Deploy to Firebase Hosting
```
firebase deploy --only hosting
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
