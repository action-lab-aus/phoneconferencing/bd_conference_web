- What happens when the host disconnects?

- Refreshing some routes (e.g. contacts) fails as it does not have props passed.

- where do we display the number for the host to tell people what number to call (for when in 'open' mode)?

- oldparticipants error in watcher in ParticipantList.vue

- some missing translations in Conference.vue

- global call recording not getting reported by firebase.
