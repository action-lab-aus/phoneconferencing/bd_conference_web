module.exports = {
  pluginOptions: {
    quasar: {
      rtlSupport: true,
    },
  },
  transpileDependencies: ["quasar"],
};
