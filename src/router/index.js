import Vue from "vue";
import VueRouter from "vue-router";
import routes from "./routes";
import { getCurrentUser, auth } from "../firebase";

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach(async (to, from, next) => 
{
  const currentUser = await getCurrentUser();
  const requiresAuth = to.matched.some((record) => record.meta.requiresAuth);
  const requiresAdmin = to.matched.some((record) => record.meta.requiresAdmin);

  if (!requiresAuth && !requiresAdmin) return next();

  if (currentUser && to.path.startsWith("/welcome")) 
  {
    return next("dashboard");
  }

  if (!currentUser) 
  {
    return next("welcome");
  }

  if (requiresAdmin) 
  {
    // uncomment to elevate the current user as an admin
    // await functions.httpsCallable("elevateCurrentUser")({
    //   Password: ''
    // });

    let token = await auth.currentUser.getIdTokenResult(true);
    if (!token.claims.admin)
    {
      // Not an admin
      return next("dashboard");
    }
  }

  return next();
});

export default router;
