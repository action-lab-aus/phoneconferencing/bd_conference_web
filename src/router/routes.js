const routes = [
  {
    path: "/",
    name: "Dashboard",
    component: () => import("../pages/Dashboard.vue"),
    meta: {
      requiresAuth: true,
    },
  },

  {
    path: "/conference/:id",
    component: () => import("../pages/Conference.vue"),
    name: "conference",
    meta: {
      requiresAuth: true,
    },
  },

  {
    path: "/schedule",
    name: "Schedule Session",
    component: () => import("../pages/ScheduleSession.vue"),
    meta: {
      requiresAuth: true,
    },
  },

  {
    path: "/upcoming/:id",
    name: "Upcoming Conference",
    component: () => import("../pages/UpcomingConference.vue"),
    meta: {
      requiresAuth: true,
    },
  },

  {
    path: "/recordings/:id",
    name: "Complete Conference",
    component: () => import("../pages/CompleteConference.vue"),
    meta: {
      requiresAuth: true,
    },
  },

  {
    path: "/begin/:id",
    name: "Begin Conference",
    component: () => import("../pages/BeginConferenceInformation.vue"),
    meta: {
      requiresAuth: true,
    },
  },

  {
    path: "/contacts",
    name: "Contacts",
    component: () => import("../pages/Contacts.vue"),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/admin/allowed",
    name: "Manage Allowed Users",
    component: () => import("../pages/admin/AllowedList.vue"),
    meta: {
      requiresAuth: true,
      requiresAdmin: true
    },
  },
  {
    path: "/admin/trunks",
    name: "Manage Trunks",
    component: () => import("../pages/admin/Trunks.vue"),
    meta: {
      requiresAuth: true,
      requiresAdmin: true
    },
  },
  {
    path: "/admin/allbilling",
    name: "Call Costing Overview",
    component: () => import("../pages/admin/CallBilling.vue"),
    meta: {
      requiresAuth: true,
      requiresAdmin: true
    },
  },
  {
    path: "/admin/adminrights",
    name: "Manage Admins",
    component: () => import("../pages/admin/AdminRights.vue"),
    meta: {
      requiresAuth: true,
      requiresAdmin: true
    },
  },
  {
    path: "/welcome",
    name: "Paroli",
    component: () => import("../pages/Splash.vue"),
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    redirect: "/",
  },
];

export default routes;
