import Vue from "vue";

import "./styles/quasar.styl";
import "./styles/proticlive.css";
import "@quasar/extras/roboto-font/roboto-font.css";
import "@quasar/extras/material-icons/material-icons.css";
import Quasar, { Notify, Loading, Dialog, Meta } from "quasar";

Vue.use(Quasar, {
  config: {},
  plugins: {
    Notify,
    Loading,
    Dialog,
    Meta,
  },
});
