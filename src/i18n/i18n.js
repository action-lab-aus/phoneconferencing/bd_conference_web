import en from "./locales/en";
import np from "./locales/np";
import bn from "./locales/bn";

const messages = {
  en: en,
  np: np,
  bn: bn
};

export default messages;
