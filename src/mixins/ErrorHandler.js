export default {
  methods: {
    handleError(e) {
      this.$q.notify({
        type: "negative",
        message: e,
        position: "top",
      });
    },
  },
};
