import Vue from "vue";
import App from "./App.vue";
import "./quasar";
import { rtdbPlugin } from "vuefire";
import router from "./router/index";

import VueApexCharts from 'vue-apexcharts'
Vue.use(VueApexCharts)
Vue.component('apexchart', VueApexCharts)

import VueCookie from 'vue-cookie';
Vue.use(VueCookie);

import i18n from "./i18n/index";

import Plugin from "@quasar/quasar-ui-qmediaplayer";
import "@quasar/quasar-ui-qmediaplayer/dist/index.css";

Vue.use(Plugin);

Vue.config.productionTip = false;

Vue.use(rtdbPlugin);

new Vue({
  router,
  i18n,
  render: (h) => h(App),
}).$mount("#app");
