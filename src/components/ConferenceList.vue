<template>
  <div :class="['container', type]">
    <span class="text-subtitle1 text-uppercase text-weight-medium title">{{ title }}</span>
    <div v-if="loading">
      <q-item class="q-px-none">
        <q-skeleton width="100%" />
      </q-item>
    </div>
    <q-list v-else-if="Object.keys(formattedConferences).length != 0" separator :class="[type]">
      <q-item clickable :class="[{ 'text-positive': type == 'Live' }, 'listItem', type + '-hoverable']"
        @click="onClick(key)" v-ripple v-for="(conference, key) in sortedConferences" :key="key">
        <q-item-section>
          <q-item-label>{{
          conference.Name || $t("conference_list.no_name")
          }}</q-item-label>
          <q-item-label caption>
            {{ formattedScheduledStartTimeFor(key) }}
          </q-item-label>
        </q-item-section>
        <q-item-section style="width: 30%">
          <q-item-label class="code">
            {{ `${$t("conference_list.code")} ${conference.PIN}` }}
          </q-item-label>
        </q-item-section>
      </q-item>
    </q-list>
    <div class="listItem text-center q-my-lg" v-else>
      {{ $t("conference_list.empty", [title]) }}
    </div>
  </div>
</template>
<script>
import { DateTime } from "luxon";
import { auth } from "../firebase";

export default {
  props: {
    conferences: {},
    creators: {
      required: true
    },
    isAdmin: {
      required: true,
      type: Boolean
    },
    loading: {
      required: true,
      type: Boolean,
    },
    onClick: {
      required: true,
      // (conferenceID: string) => void
    },
    type: {
      required: true,
      // "Upcoming" | "Live" | "Complete"
      type: String,
    },
  },
  computed: {
    title()
    {
      switch (this.type)
      {
        case "Upcoming":
          return this.$t("conference_list.upcoming_title");
        case "Live":
          return this.$t("conference_list.live_title");
        case "Complete":
          return this.$t("conference_list.complete_title");
        default:
          return "";
      }
    },
    formattedConferences()
    {
      let formatted = {};
      for (const confId in this.conferences)
      {
        let altered = this.conferences[confId];
        altered.TimeString = this.formattedScheduledStartTimeFor(confId);
        formatted[confId] = altered;
      }
      return formatted;
    },
    sortedConferences()
    {
      const sortedConfs = Object.entries(this.formattedConferences).sort(
        ([, conferenceA], [, conferenceB]) =>
        {
          return (
            (conferenceB.ActualStart || conferenceB.ScheduledStart) -
            (conferenceA.ActualStart || conferenceA.ScheduledStart)
          );
        }
      );
      return Object.fromEntries(sortedConfs);
    },
  },
  methods: {
    formattedScheduledStartTimeFor(key)
    {
      if (!this.conferences[key] || !this.conferences[key].ScheduledStart) return "ERR"

      let toRet = DateTime.fromMillis(
        this.conferences[key].ScheduledStart
      ).toLocaleString(DateTime.DATETIME_MED_WITH_SECONDS)

      if (this.isAdmin && this.conferences[key].Owner != auth.currentUser.uid)
      {
        toRet += ` | ${this.creators[this.conferences[key].Owner]}`;
      }

      return toRet;
    },
  },
};
</script>
<style scoped>
.code {
  color: grey;
  text-align: right;
  margin-right: 5px;
}

.title {
  color: white;
  margin-top: 5px;
  padding: 6px 10px 0px 10px;
}

.container {
  margin-bottom: 10px;
  border-radius: 5px;
  padding-top: 5px;
  padding-bottom: 5px;
}

.listItem {
  background: white;
  margin: 1px 5px 1px 4px;
  padding: 12px;
  border-radius: 5px;
}

.Live {
  background-color: green;
}

.Live-hoverable:hover {
  background-color: #e8f5e9;
}

.Upcoming {
  background-color: orange;
}

.Upcoming-hoverable:hover {
  background-color: #fff3e0;
}

.Complete {
  background-color: #e53935;
}

.Complete-hoverable:hover {
  background-color: #fce4ec;
}
</style>