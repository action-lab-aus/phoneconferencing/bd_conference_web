import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";
import "firebase/functions";
import "firebase/storage";

// firebase init
firebase.initializeApp({
  apiKey: "XXXXXXXXXXXXXXXXX",
  authDomain: "XXXXXXXXX.firebaseapp.com",
  databaseURL: "https://XXXXXXXXXXXXXX-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "XXXXXXXXXXXXXXXXXX",
  storageBucket: "XXXXXXXXXXXXXXXXXX.appspot.com",
  messagingSenderId: "XXXXXXXXXXXXXXXXXXX",
  appId: "XXXXXXXXXXXXXXXXXX",
  measurementId: "XXXXXXXXXX"
});

// if (process.env.NODE_ENV === "development") {
//   // use Firebase emulator for functions if running locally
//   firebase.functions().useEmulator("localhost", 5001);
// }

// utils
const db = firebase.database();
const auth = firebase.auth();
const functions = firebase.functions();
const storage = firebase.storage();

export const getCurrentUser = () =>
{
  return new Promise((resolve, reject) =>
  {
    const unsubscribe = auth.onAuthStateChanged(async (user) =>
    {
      if (user)
      {
        unsubscribe();
        resolve(user);
      } else resolve(null);
    }, reject);
  });
};

// export utils/refs
export { db, auth, functions, storage };
