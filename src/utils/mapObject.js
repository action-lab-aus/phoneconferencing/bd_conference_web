/**
 * Applies a mapping function to all of the key / value pairs
 * of an object.
 *
 * e.g. mapObject({a: 2, b: 4}, ([key, val]) => [key, val * 2])
 * returns { a: 4, b: 8 }
 *
 * @param {*} obj the object to map over
 * @param {*} mapFn the mapping function
 */
export const mapObject = (obj, mapFn) => {
  return Object.fromEntries(Object.entries(obj).map(mapFn));
};
