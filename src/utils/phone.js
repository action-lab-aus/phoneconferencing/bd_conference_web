import { parsePhoneNumberFromString } from "libphonenumber-js";

export const phoneData = [
  { code: "+355", country: "Albania (Shqipëri)" },
  { code: "+1684", country: "American Samoa" },
  { code: "+376", country: "Andorra" },
  { code: "+244", country: "Angola" },
  { code: "+1264", country: "Anguilla" },
  { code: "+1268", country: "Antigua and Barbuda" },
  { code: "+54", country: "Argentina" },
  { code: "+374", country: "Armenia (Հայաստան)" },
  { code: "+297", country: "Aruba" },
  { code: "+61", country: "Australia" },
  { code: "+43", country: "Austria (Österreich)" },
  { code: "+994", country: "Azerbaijan (Azərbaycan)" },
  { code: "+1242", country: "Bahamas" },
  { code: "+880", country: "Bangladesh (বাংলাদেশ)" },
  { code: "+1246", country: "Barbados" },
  { code: "+375", country: "Belarus (Беларусь)" },
  { code: "+32", country: "Belgium (België)" },
  { code: "+501", country: "Belize" },
  { code: "+229", country: "Benin (Bénin)" },
  { code: "+1441", country: "Bermuda" },
  { code: "+975", country: "Bhutan (འབྲུག)" },
  { code: "+591", country: "Bolivia" },
  { code: "+267", country: "Botswana" },
  { code: "+55", country: "Brazil (Brasil)" },
  { code: "+1284", country: "British Virgin Islands" },
  { code: "+673", country: "Brunei" },
  { code: "+359", country: "Bulgaria (България)" },
  { code: "+226", country: "Burkina Faso" },
  { code: "+257", country: "Burundi (Uburundi)" },
  { code: "+855", country: "Cambodia (កម្ពុជា)" },
  { code: "+237", country: "Cameroon (Cameroun)" },
  { code: "+1", country: "Canada" },
  { code: "+238", country: "Cape Verde (Kabu Verdi)" },
  { code: "+599", country: "Caribbean Netherlands" },
  { code: "+1345", country: "Cayman Islands" },
  { code: "+235", country: "Chad (Tchad)" },
  { code: "+56", country: "Chile" },
  { code: "+86", country: "China (中国)" },
  { code: "+61", country: "Christmas Island" },
  { code: "+61", country: "Cocos (Keeling) Islands" },
  { code: "+57", country: "Colombia" },
  { code: "+682", country: "Cook Islands" },
  { code: "+506", country: "Costa Rica" },
  { code: "+225", country: "Côte d’Ivoire" },
  { code: "+385", country: "Croatia (Hrvatska)" },
  { code: "+53", country: "Cuba" },
  { code: "+599", country: "Curaçao" },
  { code: "+357", country: "Cyprus (Κύπρος)" },
  { code: "+45", country: "Denmark (Danmark)" },
  { code: "+253", country: "Djibouti" },
  { code: "+1767", country: "Dominica" },
  { code: "+593", country: "Ecuador" },
  { code: "+503", country: "El Salvador" },
  { code: "+291", country: "Eritrea" },
  { code: "+372", country: "Estonia (Eesti)" },
  { code: "+251", country: "Ethiopia" },
  { code: "+298", country: "Faroe Islands (Føroyar)" },
  { code: "+679", country: "Fiji" },
  { code: "+358", country: "Finland (Suomi)" },
  { code: "+33", country: "France" },
  { code: "+241", country: "Gabon" },
  { code: "+220", country: "Gambia" },
  { code: "+995", country: "Georgia (საქართველო)" },
  { code: "+49", country: "Germany (Deutschland)" },
  { code: "+233", country: "Ghana (Gaana)" },
  { code: "+350", country: "Gibraltar" },
  { code: "+30", country: "Greece (Ελλάδα)" },
  { code: "+1473", country: "Grenada" },
  { code: "+590", country: "Guadeloupe" },
  { code: "+1671", country: "Guam" },
  { code: "+502", country: "Guatemala" },
  { code: "+44", country: "Guernsey" },
  { code: "+224", country: "Guinea (Guinée)" },
  { code: "+592", country: "Guyana" },
  { code: "+509", country: "Haiti" },
  { code: "+504", country: "Honduras" },
  { code: "+852", country: "Hong Kong (香港)" },
  { code: "+36", country: "Hungary (Magyarország)" },
  { code: "+354", country: "Iceland (Ísland)" },
  { code: "+91", country: "India (भारत)" },
  { code: "+62", country: "Indonesia" },
  { code: "+353", country: "Ireland" },
  { code: "+44", country: "Isle of Man" },
  { code: "+39", country: "Italy (Italia)" },
  { code: "+1876", country: "Jamaica" },
  { code: "+81", country: "Japan (日本)" },
  { code: "+44", country: "Jersey" },
  { code: "+7", country: "Kazakhstan (Казахстан)" },
  { code: "+254", country: "Kenya" },
  { code: "+686", country: "Kiribati" },
  { code: "+383", country: "Kosovo" },
  { code: "+996", country: "Kyrgyzstan (Кыргызстан)" },
  { code: "+856", country: "Laos (ລາວ)" },
  { code: "+371", country: "Latvia (Latvija)" },
  { code: "+266", country: "Lesotho" },
  { code: "+231", country: "Liberia" },
  { code: "+423", country: "Liechtenstein" },
  { code: "+370", country: "Lithuania (Lietuva)" },
  { code: "+352", country: "Luxembourg" },
  { code: "+853", country: "Macau (澳門)" },
  { code: "+261", country: "Madagascar (Madagasikara)" },
  { code: "+265", country: "Malawi" },
  { code: "+60", country: "Malaysia" },
  { code: "+960", country: "Maldives" },
  { code: "+223", country: "Mali" },
  { code: "+356", country: "Malta" },
  { code: "+692", country: "Marshall Islands" },
  { code: "+596", country: "Martinique" },
  { code: "+230", country: "Mauritius (Moris)" },
  { code: "+262", country: "Mayotte" },
  { code: "+52", country: "Mexico (México)" },
  { code: "+691", country: "Micronesia" },
  { code: "+377", country: "Monaco" },
  { code: "+976", country: "Mongolia (Монгол)" },
  { code: "+382", country: "Montenegro (Crna Gora)" },
  { code: "+1664", country: "Montserrat" },
  { code: "+258", country: "Mozambique (Moçambique)" },
  { code: "+95", country: "Myanmar (Burma) (မြန်မာ)" },
  { code: "+264", country: "Namibia (Namibië)" },
  { code: "+674", country: "Nauru" },
  { code: "+977", country: "Nepal (नेपाल)" },
  { code: "+31", country: "Netherlands (Nederland)" },
  { code: "+64", country: "New Zealand" },
  { code: "+505", country: "Nicaragua" },
  { code: "+227", country: "Niger (Nijar)" },
  { code: "+234", country: "Nigeria" },
  { code: "+683", country: "Niue" },
  { code: "+672", country: "Norfolk Island" },
  { code: "+1670", country: "Northern Mariana Islands" },
  { code: "+47", country: "Norway (Norge)" },
  { code: "+680", country: "Palau" },
  { code: "+507", country: "Panama (Panamá)" },
  { code: "+675", country: "Papua New Guinea" },
  { code: "+595", country: "Paraguay" },
  { code: "+51", country: "Peru (Perú)" },
  { code: "+63", country: "Philippines" },
  { code: "+48", country: "Poland (Polska)" },
  { code: "+351", country: "Portugal" },
  { code: "+1", country: "Puerto Rico" },
  { code: "+262", country: "Réunion (La Réunion)" },
  { code: "+40", country: "Romania (România)" },
  { code: "+7", country: "Russia (Россия)" },
  { code: "+250", country: "Rwanda" },
  { code: "+590", country: "Saint Barthélemy" },
  { code: "+290", country: "Saint Helena" },
  { code: "+1869", country: "Saint Kitts and Nevis" },
  { code: "+1758", country: "Saint Lucia" },
  { code: "+685", country: "Samoa" },
  { code: "+378", country: "San Marino" },
  { code: "+221", country: "Senegal (Sénégal)" },
  { code: "+381", country: "Serbia (Србија)" },
  { code: "+248", country: "Seychelles" },
  { code: "+232", country: "Sierra Leone" },
  { code: "+65", country: "Singapore" },
  { code: "+1721", country: "Sint Maarten" },
  { code: "+421", country: "Slovakia (Slovensko)" },
  { code: "+386", country: "Slovenia (Slovenija)" },
  { code: "+677", country: "Solomon Islands" },
  { code: "+252", country: "Somalia (Soomaaliya)" },
  { code: "+27", country: "South Africa" },
  { code: "+82", country: "South Korea (대한민국)" },
  { code: "+34", country: "Spain (España)" },
  { code: "+94", country: "Sri Lanka (ශ්රී ලංකාව)" },
  { code: "+597", country: "Suriname" },
  { code: "+47", country: "Svalbard and Jan Mayen" },
  { code: "+268", country: "Swaziland" },
  { code: "+46", country: "Sweden (Sverige)" },
  { code: "+41", country: "Switzerland (Schweiz)" },
  { code: "+886", country: "Taiwan (台灣)" },
  { code: "+992", country: "Tajikistan" },
  { code: "+255", country: "Tanzania" },
  { code: "+66", country: "Thailand (ไทย)" },
  { code: "+670", country: "Timor-Leste" },
  { code: "+228", country: "Togo" },
  { code: "+690", country: "Tokelau" },
  { code: "+676", country: "Tonga" },
  { code: "+1868", country: "Trinidad and Tobago" },
  { code: "+90", country: "Turkey (Türkiye)" },
  { code: "+993", country: "Turkmenistan" },
  { code: "+1649", country: "Turks and Caicos Islands" },
  { code: "+688", country: "Tuvalu" },
  { code: "+1340", country: "U.S. Virgin Islands" },
  { code: "+256", country: "Uganda" },
  { code: "+380", country: "Ukraine (Україна)" },
  { code: "+44", country: "United Kingdom" },
  { code: "+1", country: "United States" },
  { code: "+598", country: "Uruguay" },
  { code: "+998", country: "Uzbekistan (Oʻzbekiston)" },
  { code: "+678", country: "Vanuatu" },
  { code: "+58", country: "Venezuela" },
  { code: "+84", country: "Vietnam (Việt Nam)" },
  { code: "+260", country: "Zambia" },
  { code: "+263", country: "Zimbabwe" },
  { code: "+358", country: "Åland Islands" },
];

export const phoneCodes = () =>
{
  const defaultCode = process.env.VUE_APP_DEFAULT_PHONE;
  let editingData = phoneData.valueOf();

  for (let i = 0; i < editingData.length; i++)
  {
    if (editingData[i].code == defaultCode)
    {
      if (i !== 0)
      {
        editingData.unshift(editingData[i]);
      }
      break;
    }
  }

  return editingData;
};

export const uniqueCodes = () => 
{
  const uniques = new Set();

  phoneData.forEach(element =>
  {
    uniques.add(element.code)
  });

  var collator = new Intl.Collator(undefined, { numeric: true, sensitivity: 'base' });
  return Array.from(uniques).sort(collator.compare);
}

export const countriesFromNumber = (phoneNumber) =>
{
  let parsed = parseNumber(phoneNumber);
  return this.countryFromCode(parsed.countryCallingCode);
}

export const parseNumber = (phoneNumber) =>
{
  if (phoneNumber.startsWith('00'))
  {
    phoneNumber = phoneNumber.replace(/^.{2}/g, '+');
  }

  return parsePhoneNumberFromString(phoneNumber);
}

export const countriesFromCode = (code) =>
{
  let found = [];
  for (let i = 0; i < phoneData.length; i++)
  {
    let thisCode = code.startsWith('+') ? code : '+' + code;

    if (phoneData[i].code === thisCode)
    {
      found.push(phoneData[i].country);
    }
  }
  return found;
}

export const uniqueCodesWithCountries = () =>
{
  let toRet = {};

  uniqueCodes().forEach(code =>
  {
    toRet[code] = countriesFromCode(code);
  });

  return toRet;
}

export const validPhoneNumber = (phoneCode, phoneNumber) =>
{
  return (
    parseNumber(phoneCode + phoneNumber) || {
      isValid()
      {
        return false;
      },
    }
  ).isValid();
};
